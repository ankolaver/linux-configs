![Look](https://user-images.githubusercontent.com/47784720/116516197-e3636180-a8ff-11eb-8b9c-37cb8a57d7b1.png)
## Packages to install

|   Package to Install  |   Use of Package  |
| --- | --- |
| sway |  windows manager running on wayland   |
| waybar | top bar for displaying clock, cpu metrics etc. |
| playerctl | control media and volume |
| brightnessctl | control display back light brightness for laptops |
| alacritty (optional) | fast terminal emulator |
| [wlsunset](https://git.sr.ht/~kennylevinsen/wlsunset)| enable Blue-light functionality| 
| [sway launcher desktop](https://github.com/Biont/sway-launcher-desktop) | Application finder / launcher| 

## Install Location
- With exception of files ending with `.desktop` extension (which are placed inside `~/.local/share/applications`, the rest of them are placed in their respective config folders.
	- `/sway` is usually placed inside `~/.config/`
	- For me, my waybar config was placed in `/etc/xdg/waybar` but may differ based on [Waybar config location seen in the wiki](https://github.com/Alexays/Waybar/wiki/Configuration). However, it is advisable to create a `waybar` config folder in `~/.config/`.


## Sway Shortcuts
_Initially referenced from [this website](https://www.reddit.com/r/swaywm/comments/he9imx/what_are_the_keyboard_shortcuts_for_sway/)_

#### Actions

    Mod + Enter New terminal

    Mod + F Make current window fullscreen

    Mod + Shift + Q Quit program

    Mod + Shift + E Exit Sway

    Mod + Shift + C Reload Sway configuration

#### Workspaces keys

    Mod + 0..9 Change current workspace

    Mod + Shift + 0..9 Move current window to designated workspace

    Mod + E Toggle between split layouts (horizontal/vertical)

    Mod + W Tabbed layout

    Mod + A Focus on parent container

    Mod + Space Swap focus between tiling and floating

    Mod + Shift + Space Toggle floating mode

    Mod + Tab Next workspace

    Mod + Shift + Tab Previous workspace

    Mod + Left/Right/Up/Down Move focus of the window

    Mod + Shift + Left/Right/Up/Down Move the focused window in the workspace

#### Media Control
    Ctrl + Alt + g Previous Music Track
    Ctrl + Alt + h Play / Pause Music
    Ctrl + Alt + j Next Music Track
    Ctrl + Alt + k Decrease Volume
    Ctrl + Alt + l Increase Volume

#### Change Displays
    Mod + f9 Show only on second external display
    Mod + f8 External Display on top of first display
    Mod + f7 External Display on the right of first display

#### Toggle Wifi/Hotspot
    Alt + Shift + w Toggle Wifi
    Alt + Shift + h Turn on Hotspot with a given password in sway config
    Alt + Shift + g Turn off all hotspots
